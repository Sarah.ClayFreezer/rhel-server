# Installation RHEL-Server
## Installation und Partinionierung
ISO des Betriebssystems herunterladen, passend zur Architektur.
ISO auf USB-Stick **Rufus** oder **BalenaEtcher**.
BIOS-Einstellungen ändern, um vom USB-Stick booten zu können.
Auf Installationsoberfläche Benutzer **localadm** erstellen.

Falls es Probleme beim Partionieren gibt auf Konsole wechseln und mit Programm **cgdisk /dev/sdx** die Festplatten partionieren.

## Einrichtung Netzwerk
Netzwerkkarten mit `nmcli dev show` anzeigen lassen.
Bereits bestehende Verbindungen mit `nmcli con show` anzeigen lassen.
Bei Problemen bestehende Verbindungen löschen und mit `nmcli con add` eine neue Verbindung wie folgt anlegen:
`nmcli con add type ethernet con-name NAME ifname INTERFACE ipv4.address xx.xx.xx.xx/xx ipv4.gateway xx.xx.xx.xx(254) ipv4.method manual connection.autoconnect yes`

Ist alles korrekt wird eine Netzwerkverbindung hergestellt. 
Durch `ping` kann die Verbindung übeprüft werden.

## SSH Konfiguration
Mit VI-Editor die Konfiguration des SSH-Dienstes durch`sudo vi /etc/ssh/sshd_config` zur Bearbeitung aufrufen. 

Im Dienst müssen folgende Änderungen vorgenommen werden:
Mit `/` suchen, `#` entfernen, um  wirksam zu machen
``` 
Port 2022
PermitRootLogin no
```
Temporär ändern (nur temporär um später Sicherheit zu erhöhen)
```
PubkeyAuthentication yes
PasswordAuthentication yes
```
Damit Änderungen der Konfiguration übernommen werden muss der Dienst neu gestartet werden. 
```
sudo systemctl stop sshd
sudo systemctl start sshd
```
SELinux verhindert das einfache Ändern der Ports. Um die Authentifikation dafür zu gewährleisten, muss folgender Befehl ausgeführt werden (dieser findet sich in der Konfigurationsdatei des Dienstes)
`semanage port -a -t ssh_port_t -p tcp PORTNUMMER`

## Putty Einrichtung


## Benutzerkonfiguration

Benutzer können mit dem Befehl `useradd BENUTZERNAME` angelegt werden. 
Sollen sie in die Gruppe *wheel* aufgenommen muss der Befehl entsprechend erweitert werden `useradd -G wheel BENUTZERNAME`.

Eine Gruppenzuweisung zu *wheel* ist nicht immer sinnvoll, da diese Zuweisungen nicht einfach für alle Benutzer gesichtet werden können.
In diesem Fall kann eine Datei wie folgt angelegt werden, die die Rechteverwaltung ergänzt: `visudo -f /etc/sudoers.d/BENUTZERNAME`

In diese Datei wird folgendes geschrieben:

> *localadm* [TAB] *ALL=(ALL)* [TAB] *NOPASSWD:ALL*

Damit werden alle Berechtigungen zusätzlich ohne die Eingabe eines Passwortes verwaltet.





## Fehlersuche
Anzeigen von allen Log-Dateien seit dem Booten mit `journalctl -xb`
Eingrenzen der Suche durch `grep` also `journalctl -xb | grep -i xxx` um Groß- und Kleinschreibung bei der Suche nach xxx zu ignorieren

Anzeigen von Log-Dateien bestimmter Dienste `journalctl -u DIENST`

### Software
Inatallations-Pakete suchen mit `dnf search PAKETNAME`
Durchsucht Pakete nach einem Befehl `dnf provides BEFEHL`
